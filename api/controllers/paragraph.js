import paragraphService from "../services/paragraph";

const getParagraph = async (req, res) => {
  try {
    const { paragraphName } = req.params;
    const paragraph = await paragraphService.getParagraph(paragraphName);
    if (!paragraph) {
      return res.status(404).end();
    }
    return res.status(200).json(paragraph);
  } catch (error) {
    console.log(`Error on getting paragraph ${error}`);
    return res.status(500).end();
  }
};

const createParagraph = async (req, res) => {
  try {
    const { paragraphName } = req.params;
    const { numSentences } = req.body;
    const paragraph = await paragraphService.createParagraph(
      paragraphName,
      numSentences
    );
    return res.status(200).json(paragraph);
  } catch (error) {
    console.log(`Error on create paragraph ${error}`);
    return res.status(500).end();
  }
};

const addSentence = async (req, res) => {
  try {
    const { paragraphName, id } = req.params;
    const { sentence } = req.body;
    const paragraph = await paragraphService.addSentenceToParagraph(
      paragraphName,
      id,
      sentence
    );
    if (!paragraph) {
      return res.status(404).end();
    }
    if (paragraph instanceof Error) {
      return res.status(400).end();
    }
    return res.status(200).json(paragraph);
  } catch (error) {
    console.log(`Error on create paragraph ${error}`);
    return res.status(500).end();
  }
};

const deleteSentence = async (req, res) => {
  try {
    const { paragraphName, id } = req.params;
    const paragraph = await paragraphService.deleteSentence(paragraphName, id);
    return res.status(200).json(paragraph);
  } catch (error) {
    console.log(`Error on create paragraph ${error}`);
    return res.status(500).end();
  }
};

const deleteParagraph = async (req, res) => {
  try {
    const { paragraphName } = req.params;
    const paragraph = await paragraphService.deleteParagraph(paragraphName);
    return res.status(200).json(paragraph);
  } catch (error) {
    console.log(`Error on create paragraph ${error}`);
    return res.status(500).end();
  }
};

deleteParagraph;
export default {
  getParagraph,
  createParagraph,
  addSentence,
  deleteSentence,
  deleteParagraph,
};
