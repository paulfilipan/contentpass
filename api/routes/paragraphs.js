import express from "express";
import paragraphController from "../controllers/paragraph";

const router = express.Router();
router.get("/paragraph/:paragraphName", paragraphController.getParagraph);
router.post("/paragraph/:paragraphName", paragraphController.createParagraph);
router.post(
  "/paragraph/:paragraphName/sentence/:id",
  paragraphController.addSentence
);
router.delete(
  "/paragraph/:paragraphName/sentence/:id",
  paragraphController.deleteSentence
);

router.delete("/paragraph/:paragraphName", paragraphController.deleteParagraph);
export default router;
