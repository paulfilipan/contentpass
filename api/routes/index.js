import express from "express";
import status from "./status";
import paragraphs from "./paragraphs";

const router = express.Router();

const version = "v1";
router.use(`/`, status);
router.use(`/`, paragraphs);

export default router;
