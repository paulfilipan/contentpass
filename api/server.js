"use strict";
import routes from "./routes";

const bodyParser = require("body-parser");
const express = require("express");

const PORT = 8080;
const HOST = "0.0.0.0";

const app = express();
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);
app.use("/", routes);

app.listen(PORT, HOST, () => {
  console.log(`Server running on http://${HOST}:${PORT}`);
});
