import paragraphRepo from "../repo/paragraphRepo";

const createParagraph = async (name, length) => {
  return paragraphRepo.createParagraph(name, length);
};

const getParagraph = async (name) => {
  return paragraphRepo.getParagraph(name);
};

const addSentenceToParagraph = async (name, id, sentence) => {
  return paragraphRepo.addSentenceToParagraph(name, id, sentence);
};

const deleteSentence = async (name, id) => {
  return paragraphRepo.deleteSentence(name, id);
};

const deleteParagraph = async (name) => {
  return paragraphRepo.deleteParagraph(name);
};

export default {
  createParagraph,
  getParagraph,
  addSentenceToParagraph,
  deleteSentence,
  deleteParagraph,
};
