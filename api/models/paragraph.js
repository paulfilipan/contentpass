import { Schema, model } from "mongoose";

const paragraphSchema = new Schema({
  complete: {
    type: Boolean,
    required: true,
    default: false,
  },
  sentences: {
    type: Array,
  },
});
export default model("Paragraph", paragraphSchema);
