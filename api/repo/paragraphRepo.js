const paragraphs = {};
const createParagraph = async (name, length) => {
  paragraphs[name] = {
    complete: false,
    sentences: new Array(length).fill(null),
  };

  return paragraphs[name];
};

const getParagraph = async (name) => {
  return paragraphs[name];
};

const addSentenceToParagraph = async (name, id, sentence) => {
  if (paragraphs[name]) {
    if (paragraphs[name].sentences.length <= id) {
      return new Error(400);
    }
    paragraphs[name].sentences[id] = sentence;
    if (!paragraphs[name].sentences.some((el) => el === null)) {
      paragraphs[name].complete = true;
    }
    return paragraphs[name];
  }
  return undefined;
};

const deleteSentence = async (name, id) => {
  paragraphs[name].sentences[id] = null;
  if (paragraphs[name].sentences.some((el) => el === null)) {
    paragraphs[name].complete = false;
  }
  return paragraphs[name];
};

const deleteParagraph = async (name) => {
  delete paragraphs[name];
};
export default {
  createParagraph,
  getParagraph,
  addSentenceToParagraph,
  deleteSentence,
  deleteParagraph,
};
